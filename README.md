
# Password Sniffer con Python3

> El siguiente algoritmo permite obtener las credenciales de paquetes interceptados en la red a través de HTTP. La correcta ejecución de este algoritmo, va de la mano de la ejecución de un ataque ARP Spoofing, es decir, debemos vincular la ejecución de nuestro script de password sniffer con el script de envenenamiento de ARP.
> 

![Untitled](README%20md%2012274f974be84e5bb81a9f37653284b2/Untitled.png)

Básicamente, un atacante intercepta nuestras comunicaciones, captura e inspecciona los paquetes de datos que viajan en nuestra red en busca de datos sensibles como nombres de usuarios, contraseñas, tarjetas de crédito, direcciones de correo electrónico. Ahora es el equipo atacante quien controla el trafico de nuestra red, siendo realmente peligrosos en las manos equivocadas.

## Requisitos

Para realizar este ataque, se necesitan instalar las siguientes herramientas como lo son Python3 y Scapy en la máquina desde la que realizará este ataque:

*Instalación de Pyhton*

```bash
apt install python3
```

*Instalación de Scapy*

```bash
pip3 install scapy
```

Adicionalmente, debemos configurar el reenvío de IP en nuestra maquina atacante. Con el Script de apr_final.py realizaremos el ataque arp spoofing, lo que nos permitirá obtener los datos de la victima.

```bash
echo '1' > /proc/sys/net/ipv4/ip_forward
```

Un requisito fundamental es conocer la dirección IP de la victima y el Gateway de la red. Se recomienda estar en el mismo segmento de red de la victima.

## Instalación


La instalación de los script es muy sencillo, solo clone este repositorio en su maquina local y asigne los permisos correspondiente a los scripts.

```bash
chmod +x arp_final.py
chmod +x password_sniffer_final.py
```

## Uso del script password sniffer

```bash
python3 password_sniffer_final.py
```

## Realizando el ataque

1. En una terminal de Kali, ejecutamos el script de arp_final.py de la siguiente forma:

```bash
python3 arp_final.py -h

#Para obtener ayuda ejecutamos -h
```

![Untitled](README%20md%2012274f974be84e5bb81a9f37653284b2/Untitled%201.png)

```bash
python3 arp_final.py -t 192.168.46.137 --gateway 192.168.46.2

#-t : La victima.
#-g : Gateway de la red.
```

![Untitled](README%20md%2012274f974be84e5bb81a9f37653284b2/Untitled%202.png)

*Debería poder comenzar a enviar paquetes, es un indicio de la correcta ejecución del script.*

1. De forma paralela, iniciamos otra terminal en Kali, en el cual ejecutaremos el script password_sniffer_final.py

```bash
python3 password_sniffer_final.py
```

![Untitled](README%20md%2012274f974be84e5bb81a9f37653284b2/Untitled%203.png)

1. Los usuarios y contraseñas se almacenan en una archivo .json en el directorio donde están ubicados los scripts.

![Untitled](README%20md%2012274f974be84e5bb81a9f37653284b2/Untitled%204.png)

1. Para detener el ataque presione **CTRL + C**

## Créditos

Estos script han sido desarrollados en la materia Desarrollo para el Pentesting de la Especialización en Ciberseguridad de la UCM.